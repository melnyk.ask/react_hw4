import { Routes, Route } from "react-router-dom";
import Nav from "./pages/Nav";
import Home from "./pages/Home";
import Cart from "./pages/Cart";
import Favorite from "./pages/Favorite";
import NoPage from "./pages/NoPage";
// import Products from "./components/Products.js";
// import Modal from "./components/Modal.js";
// import Footer from "./components/Footer.js";
import './App.scss';
import star from "./img/star.png";
import cart from "./img/cart.png";
import { useEffect, useState } from "react";
import { getWheels } from "./features/home/homeSlice";
// import { setVisibleOn, setVisibleOff } from "./features/modal/modalSlice";
import { useDispatch } from "react-redux";

function App() {  

  let [products, setProducts] = useState([]);
  let [add_to_cart, set_add_to_cart] = useState(0);
  let [add_to_fav, set_add_to_fav] = useState(0);
  let [add_to_fav_id, set_add_to_fav_id] = useState("");
  let [star_added, set_star_added] = useState(0);
  let [visible1, set_visible1] = useState(false);
  let [shadow, set_shadow] = useState(false);

  const dispatch = useDispatch();

let actions = <div className="actions">
                <div className="ok" onClick={addToCart}>Add to cart</div>
                <div className="cancel" onClick={closeModal1}>Cancel</div>
              </div>;

  let show_add_to_cart_btn = true;
  let del_from_cart = false;
  let show_fav_star = true;
  
  function showModal() {
    set_visible1(true);
    set_shadow(true);
    // dispatch2(setVisibleOn());
    // в этой ф-ции диспатчим окрытие модалки - запускаем экшн.
  }
  // Делаем слайс для модалки
  // в слайсе - экшны. прописываем открытие модалки. закрытие модалки.

  function addToCart () {
    let increm = add_to_cart;
    set_add_to_cart(increm + 1);   
    // console.log("ID from addToCart", id);
      
    localStorage.setItem("in_cart", add_to_cart);
      // localStorage.setItem(`cart_id${id}`, true);
      //=================
      // dispatch2(setVisibleOff());
      //==================
      closeModal1();
  }

  function delFromCart () {
    let increm = add_to_cart;
    set_add_to_cart(increm - 1);   
    // console.log("ID from addToCart", id);
      
    localStorage.setItem("in_cart", add_to_cart);
      // localStorage.setItem(`cart_id${id}`, true);
  }
  
  function addToFav(id) { 
    let increm = add_to_fav;
    set_add_to_fav(increm + 1);
        
    localStorage.setItem("in_fav", add_to_fav);

    set_star_added(id);
  }
  
  function removeFromFav(id) { 
    let increm = add_to_fav;

      // if (!localStorage.getItem(`fav_id${id}`)) { 
    set_add_to_fav(increm - 1);
        
    localStorage.setItem("in_fav", add_to_fav);
      // }

    set_star_added(id);
 
  }
  
  function test(e) {
    // console.log(e.target.id);
    if (e.target.id === "test") { 
      set_visible1(false);
      set_shadow(false);
      // dispatch2(setVisibleOff());
    }
  }
  
  function closeModal1() { 
    set_visible1(false);
    set_shadow(false);
  }

  useEffect(() => { 
    // async function fetchJson() {
    //   let request = await fetch("./products.json");
    //   let response = await request.json();
    //   setProducts(response);
    //   localStorage.setItem("products", JSON.stringify(response));
    // }
    // fetchJson();

    set_add_to_cart(Number(localStorage.getItem("in_cart")));
    set_add_to_fav(Number(localStorage.getItem("in_fav")));
    set_add_to_fav_id(localStorage.getItem("in_fav_id")); 

    //=-=--=-==-=-=-=-=-=-==-
        dispatch(getWheels()); 
    //=-=-=-=-=-=-=--==-====-=-
  }, [dispatch]);
  
  useEffect(() => {   
    // console.log("update");
    localStorage.setItem("in_cart", add_to_cart);  
    localStorage.setItem("in_fav", add_to_fav);
    localStorage.setItem("in_fav_id", add_to_fav_id);
  });


  
  return (
    <>
      <Nav />
      <Routes>
        <Route path="/" index element={<Home
          test={test}
          star={star}
          cart={cart}
          add_to_cart={add_to_cart}
          add_to_fav={add_to_fav}
          actions={actions}
          closeModal1={closeModal1}
          products={products}
          showModal={showModal}
          addToFav={addToFav}
          addToCart={addToCart}
          delFromCart={delFromCart}
          removeFromFav={removeFromFav}
          star_added={star_added}
          shadow={shadow}
          visible1={visible1}
          show_add_to_cart_btn={show_add_to_cart_btn}
          del_from_cart={del_from_cart}
          show_fav_star={show_fav_star}
        />} />
        <Route path="/favorite" element={<Favorite
          show_add_to_cart_btn={show_add_to_cart_btn}
          onclick={showModal}
          onFavClick={addToFav}
          favRemove={removeFromFav}
          starAdded={star_added}
          show_fav_star={show_fav_star}
        />} />
        <Route path="/cart" element={<Cart
          del_from_cart={del_from_cart}
          addToCart={addToCart}
          delFromCart={delFromCart}
          show_fav_star={ show_fav_star}
        />} />
        <Route path="/*" element={ <NoPage />} />
      </Routes>
    </>
  );
}

export default App;
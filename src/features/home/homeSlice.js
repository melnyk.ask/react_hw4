// Slice - инициализация стэйта в сторе, инициализация функций
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const initialState = {
    // goodsInCart: 0,
    // goodsInFav: 0,
    // modalIsOpen: false,
    wheels: {},
}

export const getWheels = createAsyncThunk(
    "wheels/getWheels",
    async (_, { rejectWithValue, dispatch}) => { 
        let request = await fetch("./products.json");
        let response = await request.json(); 
        dispatch(setWheels(response));
    }
);

export const homeSlice = createSlice({
    name: "home",
    initialState,
    reducers: {
        // setGoodsInCart: (state) => {
        //     state.goodsInCart = Number(localStorage.getItem("in_cart"));
        // },
        setWheels: (state, action) => { 
            state.wheels = action.payload;
        }
    },
    // extraReducers: {
    //     [getWheels.fulfilled]: () => { console.log("ok") },
    //     [getWheels.pending]: () => { console.log("pending")},
    //     [getWheels.rejected]: () => { console.log("rejected")},
    // }
});

// export const {setGoodsInCart, setWheels} = homeSlice.actions;
export const {setWheels} = homeSlice.actions;

export default homeSlice.reducer;
import { configureStore } from "@reduxjs/toolkit";
import homeSlice from "../features/home/homeSlice";
import modalSlice from "../features/modal/modalSlice";

export const store = configureStore({
    reducer: {
        home: homeSlice,
        modal: modalSlice,
    },
});